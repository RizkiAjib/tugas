@extends('template/main')

@section('judul',"Pegawai")
@section('konten')

<body>
	<div class="container">
        <br>
        <h1 align="center" > Daftar Pegawai</h1>
        <br>   
        
        <table class="table table-success table-striped">
                <thead>
                <tr>
                    <th scope="col">No</th>
                    <th scope="col">Nip</th>
                    <th scope="col">Nama</th>
                    <th scope="col">Umur</th>
                    <th scope="col">Alamat</th>
                    <th scope="col">Level</th>
                    <th scope="col">Gaji</th>
                    <th scope="col">Aksi</th>
                </tr>
                </thead>
                <tbody>
                    
                @foreach ($datapegawai as $m)
                
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $m['nip']}}</td>
                        <td>{{ $m['nama']}}</td>
                        <td>{{ $m['umur']}}</td>
                        <td>{{ $m['alamat']}}</td>
                        <td>{{ $m['level']}}</td>
                        <td>@php
                            if ($m['level']=="A1") {
                                $gaji = 6000000 + 1500000;
                            }
                            elseif ($m['level']=="A2") {
                                $gaji = 4000000 + 100000;
                            }
                            elseif ($m['level']=="A3") {
                                $gaji = 2500000 + 500000;
                            }
                            elseif ($m['level']=="A4") {
                                $gaji = 1000000;
                            }
                        @endphp
                        {{$gaji}}</td>
                        <td><a type="button" class="btn btn-primary" href="{{ url('/pegawai/' . $m['id']) }}">Edit</a> | 
                        <td><a type="button" class="btn btn-primary" href="{{ url('/pegawai/delete/' . $m['id']) }}">Delete</a></td>
                        {{--<form action="{{url('/pegawai/' . $m['id']) }}"  method="POST">
                            @method('delete')    
                            @csrf
                            <button type="submit" class="btn btn-primary">Delete</button>
                        </form>--}}
                    </tr>  
                @endforeach
                </tbody>
            </table>
            <a href="{{ url('pegawai/create')}}" type="button" class="btn btn-primary">Tambah</a>
    </div>

    <div id="footer">
    
        <div class="footer-item">

            <div class="sosmed" align="center">
            <a href="">
                <img src="image/facebook-logo.png" alt="logo facebook">
            </a>
            <a href="">
                <img src="image/ig.png" alt="logo instagram">
            </a>
            <a href="">
                <img src="image/twitter.png" alt="logo twitter">
            </a>
            <a href="">
                <img src="image/youtube2.png" alt="logo youtube">
            </a>
            <a href="">
                <img src="image/github.png" alt="logo youtube">
            </a>
            <a href="">
                <img src="image/telegram.png" alt="logo youtube">
            </a>
            </div>
        </div>
        <div class="footer-menu">
            <ul>
            <li> <a href="index1.html"> Home </a></li>
            <li> <a href="#footer">About</a> </li>
            <li> <a href="#footer">Contact us</a> </li>
            </ul>
        </div>
        <div>
            <p>&copy; Busau 2019. by Rizki P.Aji</p>
        </div>
    
    </div>
	
    @endsection
</body>
</html>