<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PegawaiController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/pegawai', [pegawaicontroller::class, 'index']);
Route::get('/pegawai/create', [pegawaicontroller::class, 'create']);
Route::post('pegawai/', [pegawaicontroller::class, 'store']);
Route::get('pegawai/{id}', [pegawaicontroller::class, 'edit']);
Route::put('pegawai/{id}', [pegawaicontroller::class, 'update']);
Route::get('pegawai/delete/{id}', [pegawaicontroller::class, 'destroy']);
Route::delete('pegawai/{id}', [pegawaicontroller::class, 'destroy']);
